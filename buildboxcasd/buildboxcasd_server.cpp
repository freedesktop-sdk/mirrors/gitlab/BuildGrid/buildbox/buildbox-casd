/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_server.h>

#include <buildboxcasd_executionproxyinstance.h>
#include <buildboxcasd_fusestager.h>
#include <buildboxcasd_hardlinkstager.h>
#include <buildboxcasd_localacinstance.h>
#include <buildboxcasd_localacproxyinstance.h>
#include <buildboxcasd_localcasinstance.h>
#include <buildboxcasd_localcasproxyinstance.h>
#include <buildboxcasd_localexecutioninstance.h>
#include <buildboxcasd_localrainstance.h>
#include <buildboxcasd_localraproxyinstance.h>
#include <buildboxcasd_metricnames.h>

#include <buildboxcommon_exception.h>
#include <buildboxcommon_grpcerror.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_requestmetadata.h>

#include <buildboxcommonmetrics_distributionmetricutil.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_metricguard.h>

#include <uuid/uuid.h>

namespace buildboxcasd {

using namespace build::bazel::remote::execution::v2;
using namespace google::bytestream;
using namespace google::protobuf::util;
using namespace google::protobuf;
using namespace buildboxcommon;

using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::Status;

namespace {
std::shared_ptr<FileStager> createFileStager(std::shared_ptr<LocalCas> storage)
{
    std::shared_ptr<FileStager> file_stager;

    const char *stager_env = std::getenv("BUILDBOX_STAGER");
    bool auto_stager = stager_env == nullptr;
    bool fuse_stager = !auto_stager && strcmp(stager_env, "fuse") == 0;
    bool hardlink_stager = !auto_stager && strcmp(stager_env, "hardlink") == 0;

    if (!auto_stager && !fuse_stager && !hardlink_stager) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Unsupported BUILDBOX_STAGER value `"
                << stager_env
                << "`. Supported values are `fuse` and `hardlink`.");
    }

    if (auto_stager || fuse_stager) {
        try {
            file_stager = std::make_shared<FuseStager>(storage.get());
            BUILDBOX_LOG_INFO("Using `FuseStager` as staging backend")
        }
        catch (const FuseStager::StagerBinaryNotAvailable &e) {
            if (fuse_stager) {
                // FUSE was explicitly requested, don't fallback to hardlinks.
                throw e;
            }
            // The `buildbox-fuse` binary is not available,
            // fall back to employing hard links.
        }
    }

    if (!file_stager && (auto_stager || hardlink_stager)) {
        file_stager = std::make_shared<HardLinkStager>(storage.get());
        BUILDBOX_LOG_INFO("Using `HardLinkStager` as staging backend")
    }

    return file_stager;
}

void connectionOptionsForRemote(
    const Remote &remote,
    buildboxcommon::ConnectionOptions *connection_options)
{
    connection_options->d_url = remote.url().c_str();
    connection_options->d_instanceName = remote.instance_name().c_str();
    if (!remote.server_cert().empty()) {
        connection_options->d_serverCert = remote.server_cert().c_str();
    }
    if (!remote.client_key().empty()) {
        connection_options->d_clientKey = remote.client_key().c_str();
    }
    if (!remote.client_cert().empty()) {
        connection_options->d_clientCert = remote.client_cert().c_str();
    }
    if (remote.has_keepalive_time()) {
        connection_options->d_keepaliveTime =
            std::to_string(google::protobuf::util::TimeUtil::DurationToSeconds(
                remote.keepalive_time()));
    }
}

// Log the current request, the peer who initiated the request, and the
// RequestMetadata if any
void logIncomingRequest(const std::string &request_name,
                        const grpc::ServerContext *ctx)
{
    const RequestMetadata clientMetadata =
        RequestMetadataGenerator::parse_request_metadata(
            ctx->client_metadata());
    std::ostringstream outputString;
    outputString << request_name << " request from [peer=\"" << ctx->peer()
                 << "\"]: [";
    if (!(clientMetadata.tool_details().tool_name()).empty()) {
        outputString << "tool_name=\""
                     << clientMetadata.tool_details().tool_name() << "\", ";
    }
    if (!(clientMetadata.tool_details().tool_version()).empty()) {
        outputString << "tool_version=\""
                     << clientMetadata.tool_details().tool_version() << "\", ";
    }
    if (!(clientMetadata.action_id().empty())) {
        outputString << "action_id=\"" << clientMetadata.action_id() << "\", ";
    }
    if (!(clientMetadata.tool_invocation_id().empty())) {
        outputString << "tool_invocation_id=\""
                     << clientMetadata.tool_invocation_id() << "\", ";
    }
    if (!(clientMetadata.correlated_invocations_id().empty())) {
        outputString << "correlated_invocations_id=\""
                     << clientMetadata.correlated_invocations_id() << "\", ";
    }
    if (!(clientMetadata.action_mnemonic().empty())) {
        outputString << "action_mnemonic=\""
                     << clientMetadata.action_mnemonic() << "\", ";
    }
    if (!(clientMetadata.target_id().empty())) {
        outputString << "target_id=\"" << clientMetadata.target_id() << "\", ";
    }
    if (!(clientMetadata.configuration_id().empty())) {
        outputString << "configuration_id=\""
                     << clientMetadata.configuration_id() << "\", ";
    }
    outputString << "]";
    BUILDBOX_LOG_INFO(outputString.str());
}

} // namespace

Server::Server(std::shared_ptr<LocalCas> cas_storage,
               std::shared_ptr<FsLocalAssetStorage> asset_storage,
               std::shared_ptr<ActionStorage> actioncache_storage)
    : d_cas_storage(cas_storage), d_asset_storage(asset_storage),
      d_actioncache_storage(actioncache_storage),
      d_file_stager(createFileStager(cas_storage)),
      d_instance_manager(std::make_shared<InstanceManager>()),
      d_remote_execution_cas_servicer(
          std::make_shared<CasRemoteExecutionServicer>(d_instance_manager)),
      d_cas_bytestream_servicer(
          std::make_shared<CasBytestreamServicer>(d_instance_manager)),
      d_local_cas_servicer(std::make_shared<LocalCasServicer>(
          d_instance_manager, d_cas_storage, d_asset_storage,
          d_actioncache_storage, d_file_stager)),
      d_capabilities_servicer(
          std::make_shared<CapabilitiesServicer>(d_instance_manager)),
      d_asset_fetch_servicer(
          std::make_shared<AssetFetchServicer>(d_instance_manager)),
      d_asset_push_servicer(
          std::make_shared<AssetPushServicer>(d_instance_manager)),
      d_actioncache_servicer(
          std::make_shared<ActionCacheServicer>(d_instance_manager)),
      d_execution_servicer(
          std::make_shared<ExecutionServicer>(d_instance_manager)),
      d_operations_servicer(
          std::make_shared<OperationsServicer>(d_instance_manager))
{
}

Server::Server(std::shared_ptr<LocalCas> cas_storage,
               std::shared_ptr<FsLocalAssetStorage> asset_storage,
               std::shared_ptr<ActionStorage> actioncache_storage,
               const std::string &instance_name,
               const std::string &bind_address,
               const std::string &runnerCommand,
               const std::vector<std::string> &extraRunArgs, int maxJobs)
    : Server(cas_storage, asset_storage, actioncache_storage)
{
    auto cas_instance = std::make_shared<LocalCasInstance>(
        d_cas_storage, d_file_stager, instance_name);
    auto ra_instance = std::make_shared<LocalRaInstance>(d_asset_storage);
    std::shared_ptr<AcInstance> ac_instance =
        std::make_shared<LocalAcInstance>(d_actioncache_storage, cas_instance);

    std::shared_ptr<ExecutionInstance> execution_instance;
    if (!runnerCommand.empty()) {
        buildboxcommon::ConnectionOptions casd_endpoint;
        if (bind_address.find("unix:") == 0) {
            casd_endpoint.setUrl(bind_address);
        }
        else {
            casd_endpoint.setUrl("http://" + bind_address);
        }
        casd_endpoint.setInstanceName(instance_name);

        execution_instance = std::make_shared<LocalExecutionInstance>(
            casd_endpoint, runnerCommand, extraRunArgs, maxJobs);
    }

    d_instance_manager->addInstance(instance_name, cas_instance, ra_instance,
                                    ac_instance, execution_instance);
}

Server::Server(std::shared_ptr<LocalCas> cas_storage,
               std::shared_ptr<FsLocalAssetStorage> asset_storage,
               std::shared_ptr<ActionStorage> actioncache_storage,
               const buildboxcommon::ConnectionOptions &cas_endpoint,
               const buildboxcommon::ConnectionOptions *ra_endpoint,
               const buildboxcommon::ConnectionOptions *ac_endpoint,
               const buildboxcommon::ConnectionOptions *execution_endpoint,
               const std::string &instance_name, const bool read_only_remote,
               const int proxy_findmissingblobs_cache_ttl)
    : Server(cas_storage, asset_storage, actioncache_storage)
{
    auto cas_instance = std::make_shared<LocalCasProxyInstance>(
        d_cas_storage, d_file_stager, cas_endpoint, instance_name,
        read_only_remote, proxy_findmissingblobs_cache_ttl);

    std::shared_ptr<LocalRaProxyInstance> ra_instance;
    if (ra_endpoint) {
        ra_instance = std::make_shared<LocalRaProxyInstance>(d_asset_storage,
                                                             *ra_endpoint);
    }

    std::shared_ptr<AcInstance> ac_instance;
    if (ac_endpoint) {
        ac_instance = std::make_shared<LocalAcProxyInstance>(
            d_actioncache_storage, cas_instance, *ac_endpoint,
            read_only_remote);
    }

    std::shared_ptr<ExecutionInstance> execution_instance;
    if (execution_endpoint) {
        execution_instance = std::make_shared<ExecutionProxyInstance>(
            *execution_endpoint, instance_name);
    }

    d_instance_manager->addInstance(instance_name, cas_instance, ra_instance,
                                    ac_instance, execution_instance);
}

void Server::addListeningPort(const std::string &addr)
{
    if (d_server != nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::logic_error,
            "Cannot add listening port after starting server");
    }

    d_server_builder.AddListeningPort(addr, grpc::InsecureServerCredentials());
}

void Server::start()
{
    if (d_server != nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::logic_error,
                                       "Cannot start server twice");
    }

    d_server_builder.RegisterService(d_remote_execution_cas_servicer.get());
    d_server_builder.RegisterService(d_cas_bytestream_servicer.get());
    d_server_builder.RegisterService(d_local_cas_servicer.get());
    d_server_builder.RegisterService(d_capabilities_servicer.get());
    d_server_builder.RegisterService(d_asset_fetch_servicer.get());
    d_server_builder.RegisterService(d_asset_push_servicer.get());
    d_server_builder.RegisterService(d_actioncache_servicer.get());
    d_server_builder.RegisterService(d_execution_servicer.get());
    d_server_builder.RegisterService(d_operations_servicer.get());

    d_server = d_server_builder.BuildAndStart();
    if (d_server == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Failed to start server");
    }
}

void Server::shutdown()
{
    d_instance_manager->stopExecutions();
    d_server->Shutdown();
}

void Server::shutdown(gpr_timespec timeout)
{
    d_instance_manager->stopExecutions();
    d_server->Shutdown(timeout);
}

void Server::wait() { d_server->Wait(); }

void Server::addLocalServerInstance(
    const std::string &instanceName, const std::string &proxyInstanceName,
    const std::string &bindAddress, const std::string &runnerCommand,
    const std::vector<std::string> &extraRunArgs, int maxJobs)
{
    auto cas_instance = std::make_shared<LocalCasInstance>(
        d_cas_storage, d_file_stager, instanceName);

    auto ra_instance = std::make_shared<LocalRaInstance>(d_asset_storage);

    auto ac_instance =
        std::make_shared<LocalAcInstance>(d_actioncache_storage, cas_instance);

    std::shared_ptr<ExecutionInstance> execution_instance;
    if (!runnerCommand.empty()) {
        buildboxcommon::ConnectionOptions casdEndpoint;
        if (bindAddress.find("unix:") == 0) {
            casdEndpoint.setUrl(bindAddress);
        }
        else {
            casdEndpoint.setUrl("http://" + bindAddress);
        }
        casdEndpoint.setInstanceName(proxyInstanceName);

        execution_instance = std::make_shared<LocalExecutionInstance>(
            casdEndpoint, runnerCommand, extraRunArgs, maxJobs);
    }

    d_instance_manager->addInstance(instanceName, cas_instance, ra_instance,
                                    ac_instance, execution_instance);
}

/*
 * Remote Execution API: Content Addressable Storage service
 *
 */
CasRemoteExecutionServicer::CasRemoteExecutionServicer(
    std::shared_ptr<InstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
}

#define CALL_INSTANCE(service, servicer_function, instance_getter,            \
                      instance_name)                                          \
    {                                                                         \
        auto _instance =                                                      \
            this->d_instance_manager->instance_getter(instance_name);         \
        if (_instance != nullptr) {                                           \
            try {                                                             \
                return _instance->servicer_function;                          \
            }                                                                 \
            catch (const GrpcError &e) {                                      \
                return grpc::Status(e.status.error_code(),                    \
                                    std::string(__func__) + ": " + e.what()); \
            }                                                                 \
            catch (const std::runtime_error &e) {                             \
                return grpc::Status(grpc::StatusCode::INTERNAL,               \
                                    std::string(__func__) + ": " + e.what()); \
            }                                                                 \
        }                                                                     \
        else if (this->d_instance_manager->contains(instance_name)) {         \
            return grpc::Status(                                              \
                grpc::StatusCode::UNIMPLEMENTED,                              \
                service " service is not enabled for instance \"" +           \
                    instance_name + "\"");                                    \
        }                                                                     \
        else {                                                                \
            return INVALID_INSTANCE_NAME_ERROR(request, instance_name)        \
        }                                                                     \
    }

#define CALL_CAS_INSTANCE(servicer_function)                                  \
    CALL_INSTANCE("CAS", servicer_function, getCasInstance,                   \
                  request->instance_name())

#define CALL_RA_INSTANCE(servicer_function)                                   \
    CALL_INSTANCE("Remote Asset", servicer_function, getRaInstance,           \
                  request->instance_name())

#define CALL_AC_INSTANCE(servicer_function)                                   \
    CALL_INSTANCE("Action Cache", servicer_function, getAcInstance,           \
                  request->instance_name())

#define CALL_EXECUTION_INSTANCE(servicer_function, instance_name)             \
    CALL_INSTANCE("Execution", servicer_function, getExecutionInstance,       \
                  instance_name)

#define INVALID_INSTANCE_NAME_ERROR(request, instance_name)                   \
    grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,                          \
                 "Invalid instance name \"" + instance_name + "\"");

Status CasRemoteExecutionServicer::FindMissingBlobs(
    ServerContext *ctx, const FindMissingBlobsRequest *request,
    FindMissingBlobsResponse *response)
{
    logIncomingRequest("FindMissingBlobs", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_CAS_FIND_MISSING_BLOBS);

    for (const auto &digest : request->blob_digests()) {
        buildboxcommon::buildboxcommonmetrics::DistributionMetricUtil::
            recordDistributionMetric(
                MetricNames::DISTRIBUTION_NAME_CAS_FIND_MISSING_BLOBS_SIZES,
                digest.size_bytes());
    }

    CALL_CAS_INSTANCE(FindMissingBlobs(*request, response));
}

Status CasRemoteExecutionServicer::BatchUpdateBlobs(
    ServerContext *ctx, const BatchUpdateBlobsRequest *request,
    BatchUpdateBlobsResponse *response)
{
    logIncomingRequest("BatchUpdateBlobs", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_CAS_BATCH_UPDATE_BLOBS);

    for (const auto &entry : request->requests()) {
        buildboxcommon::buildboxcommonmetrics::DistributionMetricUtil::
            recordDistributionMetric(
                MetricNames::DISTRIBUTION_NAME_CAS_BATCH_UPDATE_BLOB_SIZES,
                entry.digest().size_bytes());
    }

    CALL_CAS_INSTANCE(BatchUpdateBlobs(*request, response));
}

Status CasRemoteExecutionServicer::BatchReadBlobs(
    ServerContext *ctx, const BatchReadBlobsRequest *request,
    BatchReadBlobsResponse *response)
{
    logIncomingRequest("BatchReadBlobs", ctx);

    // Checking that the blobs returned in the response do not exceed the gRPC
    // message size limit.
    // We use the maximum gRPC message size allowed to send/receive minus a
    // somewhat arbitrary value used as an estimate for the space consumed by
    // the digests and status codes embedded in the response:
    static const google::protobuf::int64 response_size_limit =
        GRPC_DEFAULT_MAX_RECV_MESSAGE_LENGTH - (1 << 16);

    google::protobuf::int64 bytes_requested = 0;
    for (const Digest &digest : request->digests()) {
        bytes_requested += digest.size_bytes();

        buildboxcommon::buildboxcommonmetrics::DistributionMetricUtil::
            recordDistributionMetric(
                MetricNames::DISTRIBUTION_NAME_CAS_BATCH_READ_BLOBS_SIZES,
                digest.size_bytes());
    }

    if (bytes_requested > response_size_limit) {
        return grpc::Status(
            grpc::StatusCode::INVALID_ARGUMENT,
            "Sum of bytes requested exceeds the gRPC message size limit: " +
                std::to_string(bytes_requested) + " > " +
                std::to_string(response_size_limit) + " bytes");
    }

    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_CAS_BATCH_READ_BLOBS);

    CALL_CAS_INSTANCE(BatchReadBlobs(*request, response));
}

Status
CasRemoteExecutionServicer::GetTree(ServerContext *ctx,
                                    const GetTreeRequest *request,
                                    ServerWriter<GetTreeResponse> *stream)
{
    logIncomingRequest("GetTree", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_CAS_GET_TREE);

    CALL_CAS_INSTANCE(GetTree(*request, stream));
}

CasBytestreamServicer::CasBytestreamServicer(
    std::shared_ptr<InstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
}

Status CasBytestreamServicer::Read(ServerContext *ctx,
                                   const ReadRequest *request,
                                   ServerWriter<ReadResponse> *writer)
{
    logIncomingRequest("Read", ctx);
    const std::string &resource_name = request->resource_name();
    std::string instance_name = readInstanceName(resource_name);

    auto instance = this->d_instance_manager->getCasInstance(instance_name);
    if (instance == nullptr) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "Invalid instance name.");
    }

    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_CAS_BYTESTREAM_READ);

    Digest requested_digest;
    const Status read_status =
        instance->Read(*request, writer, &requested_digest);

    if (requested_digest != Digest()) {
        buildboxcommon::buildboxcommonmetrics::DistributionMetricUtil::
            recordDistributionMetric(
                MetricNames::DISTRIBUTION_NAME_CAS_BYTESTREAM_READ_BLOB_SIZES,
                requested_digest.size_bytes());
    }

    return read_status;
}

Status CasBytestreamServicer::Write(ServerContext *ctx,
                                    ServerReader<WriteRequest> *request,
                                    WriteResponse *response)
{
    logIncomingRequest("Write", ctx);
    WriteRequest request_message;

    if (!request->Read(&request_message)) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "No request received.");
    }

    const std::string &resource_name = request_message.resource_name();
    std::string instance_name = writeInstanceName(resource_name);

    auto instance = this->d_instance_manager->getCasInstance(instance_name);
    if (instance == nullptr) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "Invalid instance name.");
    }

    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_CAS_BYTESTREAM_WRITE);

    Digest requested_digest;
    const Status writeStatus = instance->Write(&request_message, *request,
                                               response, &requested_digest);

    if (requested_digest != Digest()) {
        buildboxcommon::buildboxcommonmetrics::DistributionMetricUtil::
            recordDistributionMetric(
                MetricNames::DISTRIBUTION_NAME_CAS_BYTESTREAM_WRITE_BLOB_SIZES,
                requested_digest.size_bytes());
    }

    return writeStatus;
}

std::string
CasBytestreamServicer::readInstanceName(const std::string &resource_name) const
{
    return instanceNameFromResourceName(resource_name, "blobs");
}

std::string CasBytestreamServicer::writeInstanceName(
    const std::string &resource_name) const
{
    return instanceNameFromResourceName(resource_name, "uploads");
}

std::string CasBytestreamServicer::instanceNameFromResourceName(
    const std::string &resource_name, const std::string &expected_root) const
{

    // "expected_root/..."
    if (resource_name.substr(0, expected_root.size()) == expected_root) {
        return ""; // No instance name specified.
    }

    // "{instance_name}/expected_root/..."
    const auto instance_name_end = resource_name.find("/" + expected_root);
    // (According to the RE specification: "[...] the `instance_name`
    // is an identifier, possibly containing multiple path segments [...]".)
    return resource_name.substr(0, instance_name_end);
}

CapabilitiesServicer::CapabilitiesServicer(
    std::shared_ptr<InstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
    CacheCapabilities *const cache_capabilities =
        d_server_capabilities.mutable_cache_capabilities();

    cache_capabilities->add_digest_functions(CASHash::digestFunction());

    cache_capabilities->set_symlink_absolute_path_strategy(
        SymlinkAbsolutePathStrategy_Value_ALLOWED);

    // Since we are running locally, we do not limit the size of
    // batches:
    cache_capabilities->set_max_batch_total_size_bytes(0);

    ActionCacheUpdateCapabilities *const action_cache_update_capabilities =
        cache_capabilities->mutable_action_cache_update_capabilities();
    action_cache_update_capabilities->set_update_enabled(true);
}

Status
CapabilitiesServicer::GetCapabilities(grpc::ServerContext *ctx,
                                      const GetCapabilitiesRequest *request,
                                      ServerCapabilities *response)
{
    logIncomingRequest("GetCapabilities", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_CAS_GET_CAPABILITIES);

    if (this->d_instance_manager->getCasInstance(request->instance_name()) !=
        nullptr) {
        response->CopyFrom(d_server_capabilities);
        return grpc::Status::OK;
    }

    return INVALID_INSTANCE_NAME_ERROR(request, request->instance_name());
}

ActionCacheServicer::ActionCacheServicer(
    std::shared_ptr<InstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
}

Status
ActionCacheServicer::GetActionResult(ServerContext *ctx,
                                     const GetActionResultRequest *request,
                                     ActionResult *result)
{
    logIncomingRequest("GetActionResult", ctx);
    CALL_AC_INSTANCE(GetActionResult(*request, result));
}

Status ActionCacheServicer::UpdateActionResult(
    ServerContext *ctx, const UpdateActionResultRequest *request,
    ActionResult *result)
{
    logIncomingRequest("UpdateActionResult", ctx);
    CALL_AC_INSTANCE(UpdateActionResult(*request, result));
}

LocalCasServicer::LocalCasServicer(
    std::shared_ptr<InstanceManager> instance_manager,
    std::shared_ptr<LocalCas> storage,
    std::shared_ptr<FsLocalAssetStorage> asset_storage,
    std::shared_ptr<ActionStorage> actioncache_storage,
    std::shared_ptr<FileStager> file_stager)
    : d_instance_manager(instance_manager), d_storage(storage),
      d_asset_storage(asset_storage),
      d_actioncache_storage(actioncache_storage), d_file_stager(file_stager)
{
}

Status
LocalCasServicer::FetchMissingBlobs(ServerContext *ctx,
                                    const FetchMissingBlobsRequest *request,
                                    FetchMissingBlobsResponse *response)
{
    logIncomingRequest("FetchMissingBlobs", ctx);

    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_LOCAL_CAS_FETCH_MISSING_BLOB);

    for (const auto &digest : request->blob_digests()) {
        buildboxcommon::buildboxcommonmetrics::DistributionMetricUtil::
            recordDistributionMetric(
                MetricNames::DISTRIBUTION_NAME_CAS_FETCH_MISSING_BLOBS_SIZES,
                digest.size_bytes());
    }

    CALL_CAS_INSTANCE(FetchMissingBlobs(*request, response));
}

Status
LocalCasServicer::UploadMissingBlobs(ServerContext *ctx,
                                     const UploadMissingBlobsRequest *request,
                                     UploadMissingBlobsResponse *response)
{
    logIncomingRequest("UploadMissingBlobs", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_LOCAL_CAS_UPLOAD_MISSING_BLOB);

    for (const auto &digest : request->blob_digests()) {
        buildboxcommon::buildboxcommonmetrics::DistributionMetricUtil::
            recordDistributionMetric(
                MetricNames::DISTRIBUTION_NAME_CAS_UPLOAD_MISSING_BLOBS_SIZES,
                digest.size_bytes());
    }

    CALL_CAS_INSTANCE(UploadMissingBlobs(*request, response));
}

Status LocalCasServicer::FetchTree(ServerContext *ctx,
                                   const FetchTreeRequest *request,
                                   FetchTreeResponse *response)
{
    logIncomingRequest("FetchTree", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_LOCAL_CAS_FETCH_TREE);

    CALL_CAS_INSTANCE(FetchTree(*request, response));
}

Status LocalCasServicer::UploadTree(ServerContext *ctx,
                                    const UploadTreeRequest *request,
                                    UploadTreeResponse *response)
{
    logIncomingRequest("UploadTree", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_LOCAL_CAS_UPLOAD_TREE);

    CALL_CAS_INSTANCE(UploadTree(*request, response));
}

Status LocalCasServicer::StageTree(
    ServerContext *ctx,
    ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream)
{
    logIncomingRequest("StageTree", ctx);
    StageTreeRequest stage_request;
    if (!stream->Read(&stage_request)) {
        return Status(grpc::StatusCode::INTERNAL, "Could not read request.");
    }

    StageTreeRequest *request = &stage_request;

    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_LOCAL_CAS_STAGE_TREE_TOTAL);

    CALL_CAS_INSTANCE(StageTree(stage_request, stream));
}

Status LocalCasServicer::CaptureTree(ServerContext *ctx,
                                     const CaptureTreeRequest *request,
                                     CaptureTreeResponse *response)
{
    logIncomingRequest("CaptureTree", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_LOCAL_CAS_CAPTURE_TREE);

    CALL_CAS_INSTANCE(CaptureTree(*request, response));
}

Status LocalCasServicer::CaptureFiles(ServerContext *ctx,
                                      const CaptureFilesRequest *request,
                                      CaptureFilesResponse *response)
{
    logIncomingRequest("CaptureFiles", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_LOCAL_CAS_CAPTURE_FILES);

    CALL_CAS_INSTANCE(CaptureFiles(*request, response));
}

Status LocalCasServicer::GetInstanceNameForRemote(
    ServerContext *ctx, const GetInstanceNameForRemoteRequest *request,
    GetInstanceNameForRemoteResponse *response)
{
    logIncomingRequest("GetInstanceNameForRemote", ctx);
    GetInstanceNameForRemotesRequest new_request;
    GetInstanceNameForRemotesResponse new_response;

    // Transform the deprecated GetInstanceNameForRemoteRequest message into
    // the new GetInstanceNameForRemotesRequest message.
    const std::string request_str = request->SerializeAsString();
    new_request.mutable_content_addressable_storage()->ParseFromString(
        request_str);

    const auto status =
        GetInstanceNameForRemotes(ctx, &new_request, &new_response);

    response->set_instance_name(new_response.instance_name());

    return status;
}

Status LocalCasServicer::GetInstanceNameForRemotes(
    ServerContext *ctx, const GetInstanceNameForRemotesRequest *request,
    GetInstanceNameForRemotesResponse *response)
{
    logIncomingRequest("GetInstanceNameForRemotes", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_LOCAL_CAS_GET_INSTANCE_FROM_REMOTE);

    /*
     * In regular operation there is always at least one instance, however,
     * during test setup, it may be empty.
     */
    if (!this->d_instance_manager->empty()) {
        auto base_instance =
            this->d_instance_manager->getCasInstance(request->instance_name());
        if (base_instance == nullptr) {
            return INVALID_INSTANCE_NAME_ERROR(request,
                                               request->instance_name());
        }

        if (base_instance->chrooted()) {
            return grpc::Status(grpc::StatusCode::PERMISSION_DENIED,
                                "Not allowed for namespaced instances");
        }
    }

    /* Use hash of request message as instance name */
    const std::string request_str = request->SerializeAsString();
    const Digest request_digest = CASHash::hash(request_str);
    const std::string instance_name = request_digest.hash();

    auto instance = d_instance_manager->getCasInstance(instance_name);
    if (instance == nullptr) {
        /* First request for this remote, instantiate client. */

        if (!request->has_content_addressable_storage()) {
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                                "Missing CAS endpoint");
        }

        std::shared_ptr<LocalCasProxyInstance> cas_instance;
        buildboxcommon::ConnectionOptions cas_endpoint;
        connectionOptionsForRemote(request->content_addressable_storage(),
                                   &cas_endpoint);

        try {
            cas_instance = std::make_shared<LocalCasProxyInstance>(
                d_storage, d_file_stager, cas_endpoint, instance_name);
        }
        catch (const GrpcError &e) {
            /* Failed to initialize connection to server. */
            BUILDBOX_LOG_WARNING("Failed to connect to CAS server "
                                 << cas_endpoint.d_url << ": " << e.what());
            return e.status;
        }
        catch (const std::runtime_error &e) {
            BUILDBOX_LOG_WARNING("Failed to connect to CAS server "
                                 << cas_endpoint.d_url << ": " << e.what());
            return grpc::Status(grpc::StatusCode::INTERNAL,
                                std::string(__func__) + ": " + e.what());
        }

        std::shared_ptr<LocalRaProxyInstance> ra_instance;
        if (request->has_remote_asset()) {
            buildboxcommon::ConnectionOptions ra_endpoint;
            connectionOptionsForRemote(request->remote_asset(), &ra_endpoint);

            try {
                ra_instance = std::make_shared<LocalRaProxyInstance>(
                    d_asset_storage, ra_endpoint);
            }
            catch (const GrpcError &e) {
                /* Failed to initialize connection to server. */
                BUILDBOX_LOG_WARNING("Failed to connect to RA server "
                                     << ra_endpoint.d_url << ": " << e.what());
                return e.status;
            }
            catch (const std::runtime_error &e) {
                BUILDBOX_LOG_WARNING("Failed to connect to RA server "
                                     << ra_endpoint.d_url << ": " << e.what());
                return grpc::Status(grpc::StatusCode::INTERNAL,
                                    std::string(__func__) + ": " + e.what());
            }
        }

        std::shared_ptr<LocalAcProxyInstance> ac_instance;
        if (request->has_action_cache()) {
            buildboxcommon::ConnectionOptions ac_endpoint;
            connectionOptionsForRemote(request->action_cache(), &ac_endpoint);

            try {
                ac_instance = std::make_shared<LocalAcProxyInstance>(
                    d_actioncache_storage, cas_instance, ac_endpoint);
            }
            catch (const GrpcError &e) {
                /* Failed to initialize connection to server. */
                BUILDBOX_LOG_WARNING("Failed to connect to AC server "
                                     << ac_endpoint.d_url << ": " << e.what());
                return e.status;
            }
            catch (const std::runtime_error &e) {
                BUILDBOX_LOG_WARNING("Failed to connect to AC server "
                                     << ac_endpoint.d_url << ": " << e.what());
                return grpc::Status(grpc::StatusCode::INTERNAL,
                                    std::string(__func__) + ": " + e.what());
            }
        }

        d_instance_manager->addInstance(instance_name, cas_instance,
                                        ra_instance, ac_instance, nullptr);
    }

    response->set_instance_name(instance_name);

    return grpc::Status::OK;
}

Status LocalCasServicer::GetInstanceNameForNamespace(
    ServerContext *ctx, const GetInstanceNameForNamespaceRequest *request,
    GetInstanceNameForNamespaceResponse *response)
{
    logIncomingRequest("GetInstanceNameForNamespace", ctx);
    if (this->d_instance_manager->getCasInstance("") != nullptr) {
        // Deny request if there is a default instance as that would allow
        // clients to bypass the sandboxing restriction.
        return grpc::Status(grpc::StatusCode::FAILED_PRECONDITION,
                            "Namespacing not supported with default instance");
    }

    auto base_instance =
        this->d_instance_manager->getCasInstance(request->instance_name());
    if (base_instance == nullptr) {
        return INVALID_INSTANCE_NAME_ERROR(request, request->instance_name());
    }

    if (request->root().empty()) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "Invalid root path");
    }

    // Use a random UUID as new instance name
    uuid_t uu;
    uuid_generate(uu);
    std::string instance_name(36, 0);
    uuid_unparse_lower(uu, &instance_name[0]);

    std::shared_ptr<CasInstance> cas_instance(base_instance->clone());

    try {
        // Restrict cloned instance to specified root
        cas_instance->chroot(request->root());
    }
    catch (const std::runtime_error &e) {
        return grpc::Status(grpc::StatusCode::FAILED_PRECONDITION,
                            std::string(__func__) + ": " + e.what());
    }

    d_instance_manager->addInstance(instance_name, cas_instance, nullptr,
                                    nullptr, nullptr);

    response->set_instance_name(instance_name);

    return grpc::Status::OK;
}

Status LocalCasServicer::GetLocalDiskUsage(ServerContext *ctx,
                                           const GetLocalDiskUsageRequest *,
                                           GetLocalDiskUsageResponse *response)
{
    logIncomingRequest("GetLocalDiskUsage", ctx);
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_LOCAL_CAS_GET_LOCAL_DISK_USAGE);

    response->set_size_bytes(d_storage->getDiskUsage());
    response->set_quota_bytes(d_storage->getDiskQuota());

    return grpc::Status::OK;
}

AssetFetchServicer::AssetFetchServicer(
    std::shared_ptr<InstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
}

Status AssetFetchServicer::FetchBlob(ServerContext *ctx,
                                     const FetchBlobRequest *request,
                                     FetchBlobResponse *response)
{
    logIncomingRequest("FetchBlob", ctx);
    CALL_RA_INSTANCE(FetchBlob(*request, response));
}

Status AssetFetchServicer::FetchDirectory(ServerContext *ctx,
                                          const FetchDirectoryRequest *request,
                                          FetchDirectoryResponse *response)
{
    logIncomingRequest("FetchDirectory", ctx);
    CALL_RA_INSTANCE(FetchDirectory(*request, response));
}

AssetPushServicer::AssetPushServicer(
    std::shared_ptr<InstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
}

Status AssetPushServicer::PushBlob(ServerContext *ctx,
                                   const PushBlobRequest *request,
                                   PushBlobResponse *response)
{
    logIncomingRequest("PushBlob", ctx);
    CALL_RA_INSTANCE(PushBlob(*request, response));
}

Status AssetPushServicer::PushDirectory(ServerContext *ctx,
                                        const PushDirectoryRequest *request,
                                        PushDirectoryResponse *response)
{
    logIncomingRequest("PushDirectory", ctx);
    CALL_RA_INSTANCE(PushDirectory(*request, response));
}

ExecutionServicer::ExecutionServicer(
    std::shared_ptr<InstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
}

Status ExecutionServicer::Execute(ServerContext *ctx,
                                  const ExecuteRequest *request,
                                  ServerWriter<Operation> *writer)
{
    logIncomingRequest("Execute", ctx);
    CALL_EXECUTION_INSTANCE(Execute(ctx, *request, writer),
                            request->instance_name());
}

Status ExecutionServicer::WaitExecution(ServerContext *ctx,
                                        const WaitExecutionRequest *request,
                                        ServerWriter<Operation> *writer)
{
    logIncomingRequest("WaitExecution", ctx);
    size_t idx = request->name().find_first_of('#');
    std::string instance_name = "";
    if (idx != std::string::npos) {
        instance_name = request->name().substr(0, idx);
    }
    CALL_EXECUTION_INSTANCE(WaitExecution(ctx, *request, writer),
                            instance_name);
}

OperationsServicer::OperationsServicer(
    std::shared_ptr<InstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
}

Status OperationsServicer::GetOperation(ServerContext *ctx,
                                        const GetOperationRequest *request,
                                        Operation *response)
{
    logIncomingRequest("GetOperation", ctx);
    size_t idx = request->name().find_first_of('#');
    std::string instance_name = "";
    if (idx != std::string::npos) {
        instance_name = request->name().substr(0, idx);
    }
    CALL_EXECUTION_INSTANCE(GetOperation(*request, response), instance_name);
}

Status OperationsServicer::ListOperations(ServerContext *ctx,
                                          const ListOperationsRequest *request,
                                          ListOperationsResponse *response)
{
    logIncomingRequest("ListOperations", ctx);
    size_t idx = request->name().find_first_of('#');
    std::string instance_name = "";
    if (idx != std::string::npos) {
        instance_name = request->name().substr(0, idx);
    }
    CALL_EXECUTION_INSTANCE(ListOperations(*request, response), instance_name);
}

Status
OperationsServicer::CancelOperation(ServerContext *ctx,
                                    const CancelOperationRequest *request,
                                    google::protobuf::Empty *response)
{
    logIncomingRequest("CancelOperation", ctx);
    size_t idx = request->name().find_first_of('#');
    std::string instance_name = "";
    if (idx != std::string::npos) {
        instance_name = request->name().substr(0, idx);
    }
    CALL_EXECUTION_INSTANCE(CancelOperation(*request, response),
                            instance_name);
}

} // namespace buildboxcasd
