/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_runner.h>

#include <string>
#include <vector>

class TestRunner : public buildboxcommon::Runner {
  public:
    buildboxcommon::ActionResult
    execute(const buildboxcommon::Command &command,
            const buildboxcommon::Digest &inputRootDigest) override
    {
        buildboxcommon::ActionResult result;
        const std::vector<std::string> commandLine(
            command.arguments().cbegin(), command.arguments().cend());

        executeAndStore(commandLine, &result);

        return result;
    }
};

BUILDBOX_RUNNER_MAIN(TestRunner)
