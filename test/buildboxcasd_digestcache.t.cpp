/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_digestcache.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_protos.h>

#include <gtest/gtest.h>

using buildboxcommon::CASHash;
using buildboxcommon::Digest;

TEST(FindMissingBlobsCache, Empty)
{
    buildboxcasd::DigestCache cache;
    ASSERT_FALSE(cache.hasDigest(Digest()));
    ASSERT_FALSE(cache.hasDigest(CASHash::hash("test")));
}

TEST(FindMissingBlobsCache, Insert)
{
    buildboxcasd::DigestCache cache;

    const Digest digest = CASHash::hash("test");
    ASSERT_FALSE(cache.hasDigest(digest));
    ASSERT_TRUE(cache.addDigest(digest));
    ASSERT_TRUE(cache.hasDigest(digest));
}
