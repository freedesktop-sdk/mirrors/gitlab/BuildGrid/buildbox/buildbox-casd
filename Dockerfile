FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest
COPY . /casd
RUN cd /casd && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=DEBUG ..
RUN cd /casd/build && make -j install
RUN cd /casd/build && ctest --verbose
